package br.edu.up;

import br.edu.up.models.Ponto;
import br.edu.up.utils.Prompt;

public class Program {

    public static void main(String[] args) {

        double distancia;

        Ponto ponto1 = new Ponto();
        Ponto ponto2 = new Ponto(2, 5);

        distancia = ponto1.calcularDistancia(ponto2);

        Prompt.separador();
        Prompt.imprimir("ponto1 (" + ponto1.getX() + ", " + ponto1.getY() + ")");
        Prompt.imprimir("ponto2 (" + ponto2.getX() + ", " + ponto2.getY() + ")");
        Prompt.separador();

        Prompt.imprimir("A distancia entre o ponto1 (0,0) e o ponto2 (2,5) é de " + distancia);
        distancia = ponto2.calcularDistancia(7, 2);
        Prompt.imprimir("A distancia entre o ponto2 (2,5) e às coordenadas (7,2) é de " + distancia);

        Prompt.separador();
        ponto1.setX(10);
        ponto1.setY(3);
        Prompt.imprimir("a nova posição do ponto1 é (" + ponto1.getX() + ", " + ponto1.getY() + ")");
        Prompt.separador();

    }
}